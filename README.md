## Setup
- npm i
## Run app

- npm start

## Run test

- npm run test

# Assignment
### I, Javascript algorithm
```Provide an array of strings, eg: [‘a’, ‘ab’, ‘abc’, ‘cd’]. Write a function to find the strings’ length that appear most in this array. Writing the unit test function and provide some test-cases.
Provide an array of integers, eg: [1, 4, 2, 3, 5]. Write a function to find sum of integers on top 2. Writing the unit test function and provide some test-cases.```
