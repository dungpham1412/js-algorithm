export const nbAppearOfItemInArray = (array) => {
  return array.reduce((acc, currentValue) => {
      const newAcc = {...acc};
      newAcc[currentValue] = acc[currentValue] ? acc[currentValue] + 1 : 1;
      return newAcc;
  }, {})
}

export const  sumOfIntegerOnTop2 = (array) => {
  return array.reduce((acc, cur) => acc + ((/^(2)\d*$/).test(cur) ? cur : 0), 0)
}