import { nbAppearOfItemInArray, sumOfIntegerOnTop2 } from '../index';

describe('# nbAppearOfItemInArray()', () => {
	it(`Should return result is a object has key is each the element of the input array
	and value is the number of occurrences of the element in the array`, () => {
		const input = ['dd', 'du', 'dun', 'dd', 'dung', 'dddduuunnnggg', 'du'];
		const result = nbAppearOfItemInArray(input);
		expect(result).toEqual({"dd": 2, "dddduuunnnggg": 1, "du": 2, "dun": 1, "dung": 1})
	})
});

describe('# sumOfIntegerOnTop2()', () => {
	it(`Should return result is sum of integers on top 2`, () => {
		const input = [1, 2, 3, 23, 4, 200, 202, 101];
		const result = sumOfIntegerOnTop2(input);
		expect(result).toEqual(427)
	});
	it(`Should return 0 if have no integers on top 2`, () => {
		const input = [1, 3, 5, 7, 9, 11];
		const result = sumOfIntegerOnTop2(input);
		expect(result).toEqual(0)
	});
});