## Step 1

- npm i
- node v8.11.3

## Run app

- npm start

## Run unit test

- npm run test

## Run test cucumber

- npm run start:contract
- npm run test-feature

## Run coverage

- npm run coverage